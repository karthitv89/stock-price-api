# Stock Price Api #
This project serves as a backend for the Stock Price app. We have create only one microservice which contains both the webController and business logic. In this project we are exposing services to store and retrieve stock price data.


## Technologies ##

    1. Spring Boot
    2. Java 11
    3. Maven
    4. JUnit
    5. Mockito
    6. H2 in memory database
    7. Spring Data
    8. Lombok

## Controller: StockPriceController ##
    URIs exposed:
    1. /stockPrice/bulkCreate
        This api takes stock json array as input and then validate and stores in the database. This will be used by Reuter batches.
    2. /stockPrice/latestList
        This api retrieves lastest stocks to be display in the UI.

## Services ##
- StockPriceService : Handles all the business logic for validating, storing Stocks. Also, this is used to get the latest stock records.

## Test cases ##
- Test cases where written all the scenarios.
- Test cases written for each layer. That is, test case where written for Controller, Service and Repository layers.
- Mockito used for testing
- Test coverage

        Folder - com
        Classes covered - 100% (6/6)
        Methods covered - 79% (19/24) (some temp testing methods were not covered.
        Lines covered - 82% (60/73)

### How do I get set up? ###
* Dependencies
    - Install Java 11, Maven
* Database configuration
    - H2 in memory database.
* How to run tests
    - mvn clean test
* Deployment instructions
    - Run mvn clean install
    - mvn spring-boot:run
* Version
    - 0.0.1-SNAPSHOT
