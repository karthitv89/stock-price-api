package com.karthik.stockPriceApi.repository;

import com.karthik.stockPriceApi.AppConstants;
import com.karthik.stockPriceApi.persist.Stock;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(SpringExtension.class)
@DataJpaTest
class StockRepositoryTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private StockRepository stockRepository;

    @Test
    private void contextLoads() {
        assertThat(entityManager).isNotNull();
        assertThat(stockRepository).isNotNull();
    }

    @BeforeEach
    void setUp() {
        double bidPrice = 14.00;
        double askPrice = 16.00;
        String symbol = "ABC:SGX";
        Stock stock = getStock(bidPrice, askPrice, symbol);
        entityManager.persist(stock);
        entityManager.flush();
    }

    private Stock getStock(Double bidPrice, Double askPrice, String symbol) {
        Stock stock = new Stock();
        stock.setTrend(AppConstants.TREND_UP);
        stock.setMarketPrice((bidPrice + askPrice) / 2);
        stock.setBidPrice(bidPrice);
        stock.setAskPrice(askPrice);
        stock.setEventTime(new Date());
        stock.setSymbol(symbol);
        return stock;
    }

    private Stock getStock(Double bidPrice, Double askPrice, String symbol, String trend) {
        Stock stock = new Stock();
        stock.setTrend(trend);
        stock.setMarketPrice((bidPrice + askPrice) / 2);
        stock.setBidPrice(bidPrice);
        stock.setAskPrice(askPrice);
        stock.setEventTime(new Date());
        stock.setSymbol(symbol);
        return stock;
    }

    @AfterEach
    void tearDown() {
        entityManager.clear();
    }

    @Test
    void findAll() {
        Assertions.assertEquals(1, stockRepository.findAll().size());
        entityManager.persist(getStock(13.00, 17.00, "DEF:SGX"));
        entityManager.flush();
        Assertions.assertEquals(2, stockRepository.findAll().size());
    }

    @Test
    void findAllTopBySymbolOrderByEventTime() {
        entityManager.persist(getStock(13.00, 17.00, "DEF:SGX"));
        entityManager.persist(getStock(12.00, 18.00, "GHI:SGX"));
        entityManager.persist(getStock(11.00, 17.00, "DEF:SGX"));
        entityManager.persist(getStock(11.00, 17.00, "JKL:SGX"));
        Assertions.assertEquals(2, stockRepository.
                findAllTopBySymbolOrderByEventTime(Arrays.asList("DEF:SGX", "GHI:SGX", "XXX")).size()); // XXX not available in db
        Assertions.assertEquals(3, stockRepository.
                findAllTopBySymbolOrderByEventTime(Arrays.asList("DEF:SGX", "GHI:SGX", "JKL:SGX")).size());
    }

    @Test
    void fetchLatestStockDetails() {
        entityManager.persist(getStock(13.00, 17.00, "DEF:SGX", AppConstants.TREND_UP));
        entityManager.persist(getStock(12.00, 18.00, "GHI:SGX", AppConstants.TREND_UP)); // New record. UP
        entityManager.persist(getStock(11.00, 14.00, "DEF:SGX", AppConstants.TREND_DOWN)); // market price reduced
        entityManager.persist(getStock(18.00, 21.00, "ABC:SGX", AppConstants.TREND_UP)); // Market price increased
        List<Stock> latestStockRecords;
        latestStockRecords = stockRepository.fetchLatestStockDetails(PageRequest.of(0, 50
                , Sort.by(Sort.Direction.DESC, "event_time")));
        Assertions.assertEquals(3, latestStockRecords.size());
        Assertions.assertTrue(
                latestStockRecords.stream()
                        .filter(stk -> stk.getSymbol().equals("ABC:SGX"))
                        .findFirst()
                        .get()
                        .getTrend().equals(AppConstants.TREND_UP));
        Assertions.assertTrue(
                latestStockRecords.stream()
                        .filter(stk -> stk.getSymbol().equals("DEF:SGX"))
                        .findFirst()
                        .get()
                        .getTrend().equals(AppConstants.TREND_DOWN));
    }
}