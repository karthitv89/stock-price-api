package com.karthik.stockPriceApi.service.impl;

import com.karthik.stockPriceApi.AppConstants;
import com.karthik.stockPriceApi.ApplicationException;
import com.karthik.stockPriceApi.persist.Stock;
import com.karthik.stockPriceApi.repository.StockRepository;
import com.karthik.stockPriceApi.service.StockService;
import org.aspectj.lang.annotation.Before;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@SpringBootTest
class StockServiceImplTest {

    @Autowired
    private StockService stockService;

    @MockBean
    private StockRepository stockRepository;

    @BeforeEach
    void setUp() {
        mockStockList();

        /*List<Stock> stocks = new ArrayList<>();

        double bidPrice = 12.00, askPrice = 16.00;
        String symbol = "ABC";
        for(int i = 1; i<=5; i++) {
            stocks.add(getStock(bidPrice + 1, askPrice + 1, symbol + i));
        }

        bidPrice = 15.00;
        askPrice = 18.00;
        for(int i = 1; i<=3; i++) {
            stocks.add(getStock(bidPrice + i, askPrice + i, symbol + i));
        }


        bidPrice = 11.00;
        askPrice = 18.00;
        for(int i = 4; i<=5; i++) {
            stocks.add(getStock(bidPrice + i, askPrice + i, symbol + i));
        }
        stockService.bulkCreate(stocks);
        stockRepository.saveAll(stocks);*/
    }

    private void mockStockList() {
        List<String> symbolList = getSymbolList();
        double bidPrice = 12.00, askPrice = 16.00;
        String symbol = "ABC";
        List<Stock> stockList = getStocks(bidPrice, askPrice, symbol);

        Mockito.when(stockRepository.findAllTopBySymbolOrderByEventTime(symbolList)).thenReturn(stockList);
    }


    private Stock getStock(Double bidPrice, Double askPrice, String symbol) {
        Stock stock = new Stock();
        stock.setTrend(AppConstants.TREND_UP);
        stock.setMarketPrice((bidPrice + askPrice) / 2);
        stock.setBidPrice(bidPrice);
        stock.setAskPrice(askPrice);
        stock.setEventTime(new Date());
        stock.setSymbol(symbol);
        return stock;
    }

    private Stock getStock(Double bidPrice, Double askPrice, String symbol, String trend) {
        Stock stock = new Stock();
        stock.setTrend(trend);
        stock.setMarketPrice((bidPrice + askPrice) / 2);
        stock.setBidPrice(bidPrice);
        stock.setAskPrice(askPrice);
        stock.setEventTime(new Date());
        stock.setSymbol(symbol);
        return stock;
    }



    @AfterEach
    void tearDown() {
    }

    @Test
    void validateStock() {
        Exception exception = Assertions.assertThrows(ApplicationException.class, () -> {
            List<Stock> newStockList = new ArrayList<>();
            newStockList.add(getStock(21.00,19.00, ""));
            newStockList = stockService.bulkCreate(newStockList);
        });
        String expectedMessage = "Validation fails";
        String actualMessage = exception.getMessage();

        Assertions.assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    void bulkCreate_withExistingRecords_TrendUp() {
        double bidPrice = 12.00, askPrice = 16.00;
        String symbol = "ABC";

        List<Stock> newStockList = getStocks(bidPrice, askPrice, symbol);

        newStockList = stockService.bulkCreate(newStockList);

        Assertions.assertEquals(0, newStockList.stream()
                .filter(stk -> stk.getTrend().equals(AppConstants.TREND_DOWN))
                .count());
        Assertions.assertEquals(2, newStockList.stream()
                .filter(stk -> stk.getTrend().equals(AppConstants.TREND_UP))
                .count());
    }

    @Test
    void bulkCreate_withExistingRecords_TrendDown() {
        double bidPrice = 12.00, askPrice = 16.00;
        String symbol = "ABC";

        List<Stock> newStockList = new ArrayList<>();
        for (int i = 1; i <= 2; i++) {
            newStockList.add(getStock(bidPrice - i, askPrice - i, symbol + i));
        }

        newStockList = stockService.bulkCreate(newStockList);

        Assertions.assertEquals(0, newStockList.stream()
                .filter(stk -> stk.getTrend().equals(AppConstants.TREND_UP))
                .count());
        Assertions.assertEquals(2, newStockList.stream()
                .filter(stk -> stk.getTrend().equals(AppConstants.TREND_DOWN))
                .count());
    }

    private List<Stock> getStocks(double bidPrice, double askPrice, String symbol) {
        List<Stock> stockList = new ArrayList<>();

        for (int i = 1; i <= 2; i++) {
            stockList.add(getStock(bidPrice + i, askPrice + i, symbol + i));
        }
        return stockList;
    }


    @Test
    void bulkCreate_withExistingRecords_TrendMixed() {
        List<String> symbolList = getSymbolList();
        double bidPrice = 12.00, askPrice = 16.00;

        List<Stock> newStockList = new ArrayList<>();
        newStockList.add(getStock(bidPrice - 2, askPrice - 2, "ABC1"));
        newStockList.add(getStock(bidPrice + 5, askPrice + 5, "ABC2"));

        newStockList = stockService.bulkCreate(newStockList);

        Assertions.assertEquals(AppConstants.TREND_DOWN, newStockList.stream()
                .filter(stk -> stk.getSymbol().equals("ABC1")).findFirst().get().getTrend());
        Assertions.assertEquals(AppConstants.TREND_UP, newStockList.stream()
                .filter(stk -> stk.getSymbol().equals("ABC2")).findFirst().get().getTrend());
    }

    private List<String> getSymbolList() {
        return Stream.of("ABC1", "ABC2").collect(Collectors.toList());
    }

    @Test
    void fetchLatestStockDetails() {
        Pageable pageable = PageRequest.
                of(0, 50, Sort.by(Sort.Direction.DESC, "event_time"));

        //Mockito.when(stockRepository.fetchLatestStockDetails(pageable)).thenReturn()
        List<Stock> latestStocks = stockService.fetchLatestStockDetails(120);
        System.out.println(latestStocks);
    }
}