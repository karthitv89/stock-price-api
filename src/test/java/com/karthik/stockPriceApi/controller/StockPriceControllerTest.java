package com.karthik.stockPriceApi.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.karthik.stockPriceApi.AppConstants;
import com.karthik.stockPriceApi.persist.Stock;
import com.karthik.stockPriceApi.service.StockService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.MockReset;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(StockPriceController.class)
class StockPriceControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private StockService service;

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    void testBulkCreate() throws Exception {
        MvcResult mvcResult = mockMvc.perform(post("/stockPrice/bulkCreate")
                .contentType("application/json")
                .content(objectMapper.writeValueAsString(getStocks(12.00, 16.00, "ABC"))))
                .andExpect(status().isOk()).andReturn();

    }

    @Test
    void testListLatest() throws Exception {
        MvcResult mvcResult = mockMvc.perform(get("/stockPrice/latestList")
                .param("size", "20"))
                .andExpect(status().isOk()).andReturn();

    }

    private List<Stock> getStocks(double bidPrice, double askPrice, String symbol) {
        List<Stock> stockList = new ArrayList<>();

        for (int i = 1; i <= 2; i++) {
            stockList.add(getStock(bidPrice + i, askPrice + i, symbol + i));
        }
        return stockList;
    }

    private Stock getStock(Double bidPrice, Double askPrice, String symbol) {
        Stock stock = new Stock();
        stock.setTrend(AppConstants.TREND_UP);
        stock.setMarketPrice((bidPrice + askPrice) / 2);
        stock.setBidPrice(bidPrice);
        stock.setAskPrice(askPrice);
        stock.setEventTime(new Date());
        stock.setSymbol(symbol);
        return stock;
    }
}