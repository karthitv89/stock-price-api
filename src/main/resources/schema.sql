
CREATE DATABASE StockPriceDb;

CREATE SCHEMA stock_price
    AUTHORIZATION postgres;

CREATE TABLE IF DOES NOT EXISTS stock_price.stock
(
    id integer NOT NULL,
    ask_price double precision NOT NULL,
    bid_price double precision NOT NULL,
    event_time timestamp without time zone,
    market_price double precision NOT NULL,
    symbol character varying(255) COLLATE pg_catalog."default" NOT NULL,
    trend character varying(255) COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT stock_pkey PRIMARY KEY (id)
)

TABLESPACE pg_default;

ALTER TABLE stock_price.stock
    OWNER to postgres;