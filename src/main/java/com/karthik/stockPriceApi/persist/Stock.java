package com.karthik.stockPriceApi.persist;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

@Data
@NoArgsConstructor
@Entity
@Table(
        name = "stock")
public class Stock {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @Column(name = "symbol", nullable = false)
    private String symbol;

    @Column(name = "market_price", nullable = false)
    private Double marketPrice;

    @Column(name = "bid_price", nullable = false)
    private Double bidPrice;

    @Column(name = "ask_price", nullable = false)
    private Double askPrice;

   @Column(name = "trend", nullable = false)
    private String trend;

    @Column(name="event_time")
    @Getter
    private Date eventTime;

}
