package com.karthik.stockPriceApi.controller;

import com.karthik.stockPriceApi.persist.Stock;
import com.karthik.stockPriceApi.service.StockService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/stockPrice")
public class StockPriceController {

    @Autowired
    private StockService stockService;

    @PostMapping(value = "/bulkCreate")
    public ResponseEntity<List<Stock>> bulkCreate(@RequestBody List<Stock> stocks) {
        List<Stock> stockList = null;
        try {
            stockList = stockService.bulkCreate(stocks);

        } catch (Exception ex) {
            new ResponseEntity<>(ex.getMessage(), HttpStatus.UNPROCESSABLE_ENTITY);
        }
        return new ResponseEntity<>(stockList, HttpStatus.OK);
    }

    @GetMapping(value= "/list") // FOR TESTING
    private List<Stock> list(@RequestParam int size) {
        return this.stockService.list(size);
    }

    @GetMapping(value = "/latestList")
    private List<Stock> listLatest(@RequestParam Optional<Integer> sizeOptional) {
        return this.stockService.fetchLatestStockDetails(sizeOptional.isPresent() ? sizeOptional.get() : 100);
    }
}
