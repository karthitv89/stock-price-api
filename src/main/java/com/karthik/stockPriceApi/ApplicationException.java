package com.karthik.stockPriceApi;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class ApplicationException extends RuntimeException {

    public ApplicationException(String errorMessage) {
        super(errorMessage);
    }

    public ApplicationException(String message, Throwable cause) {
        super(message, cause);
    }
}
