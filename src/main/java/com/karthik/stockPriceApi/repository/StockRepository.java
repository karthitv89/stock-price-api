package com.karthik.stockPriceApi.repository;

import com.karthik.stockPriceApi.persist.Stock;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository(value="stockRepository")
public interface StockRepository extends JpaRepository<Stock,Integer> {

    public Page<Stock> findAll(Pageable pageable);

    @Query(value = "WITH summary AS (\n" +
            "    SELECT s.id, \n" +
            "        s.symbol, \n" +
            "        s.ask_price,\n" +
            "\t\ts.bid_price,\n" +
            "\t\ts.market_price,\n" +
            "\t\ts.event_time, \n" +
            "\t\ts.trend, \n" +
            "        ROW_NUMBER() OVER(PARTITION BY s.symbol \n" +
            "                                 ORDER BY s.event_time DESC) AS rk\n" +
            "\n" +
            "      FROM stock s WHERE symbol IN (:symbolList))\n" +
            "SELECT s.*\n" +
            "  FROM summary s\n" +
            " WHERE s.rk = 1", nativeQuery = true)
    List<Stock> findAllTopBySymbolOrderByEventTime(List<String> symbolList);


    @Query(value = "WITH summary AS (\n" +
            "    SELECT s.id, \n" +
            "        s.symbol, \n" +
            "        s.ask_price,\n" +
            "\t\ts.bid_price,\n" +
            "\t\ts.market_price,\n" +
            "\t\ts.event_time, \n" +
            "\t\ts.trend, \n" +
            "        ROW_NUMBER() OVER(PARTITION BY s.symbol \n" +
            "                                 ORDER BY s.event_time DESC) AS rk\n" +
            "\n" +
            "      FROM stock s )\n" +
            "SELECT s.*\n" +
            "  FROM summary s\n" +
            " WHERE s.rk = 1 ", nativeQuery = true)
    List<Stock> fetchLatestStockDetails(Pageable pageable);

    void findBySymbol(String s);
}
