package com.karthik.stockPriceApi.service.impl;

import com.karthik.stockPriceApi.AppConstants;
import com.karthik.stockPriceApi.ApplicationException;
import com.karthik.stockPriceApi.persist.Stock;
import com.karthik.stockPriceApi.repository.StockRepository;
import com.karthik.stockPriceApi.service.StockService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service("stockService")
public class StockServiceImpl implements StockService {

    @Autowired
    private StockRepository stockRepository;

    @Override
    public List<Stock> bulkCreate(List<Stock> stockList) {
        stockList.stream().forEach(stock -> {
            String validationError = validateStock(stock);
            if (!StringUtils.isEmpty(validationError)) {
                throw new ApplicationException("Validation fails : Symbol: " + stock.getSymbol() + " : " + validationError);
            }
        });
        List<String> symbolList = stockList.stream().map(stock -> stock.getSymbol()).collect(Collectors.toList());
        List<Stock> existingStocks = stockRepository.findAllTopBySymbolOrderByEventTime(symbolList);
        Map<String, Stock> existingStocksMap = existingStocks.stream().collect(Collectors.toMap(Stock::getSymbol, Function.identity()));
        stockList.stream().forEach(stock -> {
            stock.setMarketPrice((stock.getAskPrice() + stock.getBidPrice()) / 2);
            if (existingStocksMap.containsKey(stock.getSymbol())) {
                Stock extStk = existingStocksMap.get(stock.getSymbol());
                if (extStk.getMarketPrice() <= stock.getMarketPrice()) { // no change in market price taken as UP
                    stock.setTrend(AppConstants.TREND_UP);
                } else {
                    stock.setTrend(AppConstants.TREND_DOWN);
                }
            } else {
                stock.setTrend(AppConstants.TREND_UP);
            }
        });
        this.stockRepository.saveAll(stockList);
        return stockList;
    }

    private String validateStock(Stock stock) {
        StringBuilder sb = new StringBuilder();
        if (StringUtils.isEmpty(stock.getSymbol())) {
            sb.append("Stock symbol cannot be null. ");
        }
        if (stock.getBidPrice() > stock.getAskPrice()) {
            sb.append("Bid price cannot greater than ask price. ");
        }

        if (stock.getEventTime() == null) {
            sb.append("Event time cannot be null. ");
        }
        return sb.toString();
    }

    @Override
    public List<Stock> list(int size) {
        Pageable pageable = PageRequest.
                of(0, size, Sort.by(Sort.Direction.DESC, "eventTime"));
        return this.stockRepository.findAll(pageable).getContent();
    }

    @Override
    public List<Stock> fetchLatestStockDetails(int size) {
        Pageable pageable = PageRequest.
                of(0, size, Sort.by(Sort.Direction.DESC, "event_time"));
        return this.stockRepository.fetchLatestStockDetails(pageable);
    }


}
