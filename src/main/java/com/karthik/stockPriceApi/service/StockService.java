package com.karthik.stockPriceApi.service;

import com.karthik.stockPriceApi.persist.Stock;

import java.util.List;

public interface StockService {
    public List<Stock> bulkCreate(List<Stock> stockList);

    List<Stock> list(int size);

    List<Stock>  fetchLatestStockDetails(int size);
}
